from django.db import models


# Create your models here.
class Client(models.Model):
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    observations = models.CharField(max_length=45, blank=True)
    delete_client = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.first_name} {self.last_name}  {self.observations}'


class Waiter(models.Model):
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    last_name2 = models.CharField(max_length=45)
    delete_waiter = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.first_name}  {self.last_name}'


class Table(models.Model):
    num_clients = models.IntegerField()
    location = models.CharField(max_length=45)
    delete_table = models.BooleanField(default=False)

    def __str__(self):
        return self.location


class TypeProduct(models.Model):
    type = models.CharField(max_length=45)
    delete_type_product = models.BooleanField(default=False)

    def __str__(self):
        return self.type


class Product(models.Model):
    name = models.CharField(max_length=45)
    type = models.ForeignKey(TypeProduct, on_delete=models.CASCADE)
    importer = models.FloatField(max_length=45)
    delete_product = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Facture(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    id_client = models.ForeignKey(Client, on_delete=models.CASCADE)
    id_waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    id_table = models.ForeignKey(Table, on_delete=models.CASCADE)
    delete_facture = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)


class Order(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    id_facture = models.ForeignKey(Facture, on_delete=models.CASCADE)
    id_table = models.ForeignKey(Table, on_delete=models.CASCADE)
    delete_order = models.BooleanField(default=False)

    def __str__(self):
        return self.product.name
