from django.apps import AppConfig


class RincodelsaborConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rinconDelSabor'
