from rest_framework import serializers
from rinconDelSabor.models import Client, Waiter, Table, TypeProduct, Product, Facture, Order


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = [
            'first_name',
            'last_name',
            'observations',
            'delete_client'
        ]


class WaiterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Waiter
        fields = [
            'first_name',
            'last_name',
            'last_name2',
            'delete_client'
        ]


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table
        fields = [
            'num_clients',
            'location',
            'delete_table'
        ]


class TypeProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypeProduct
        fields = [
            'type',
            'delete_type_product',
        ]


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'name',
            'type',
            'importer',
            'delete_product',
        ]


class FactureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facture
        fields = [
            'date',
            'id_client',
            'id_waiter',
            'id_table',
            'delete_facture',
        ]


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = [
            'product',
            'quantity',
            'id_facture',
            'id_table',
            'delete_order',
        ]
