import order as order
from django.contrib import admin

# Register your models here.
from rinconDelSabor.models import Client, Waiter, Table, Facture, TypeProduct, Product, Order

admin.site.register(Client)
admin.site.register(Waiter)
admin.site.register(TypeProduct)
admin.site.register(Product)
admin.site.register(Table)
admin.site.register(Order)


@admin.register(Facture)
class FactureAdmin(admin.ModelAdmin):
    list_display = (
        'id_client',
        'id_waiter',
        'id_table'
    )
