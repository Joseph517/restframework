from django.urls import path, include
from rinconDelSabor import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'client', views.ClientViewsets)
router.register(r'waiter', views.WaiterViewsets)
router.register(r'table', views.TableViewsets)
router.register(r'typeProduct', views.TypeProductViewsets)
router.register(r'product', views.ProductViewsets)
router.register(r'facture', views.FactureViewsets)
router.register(r'order', views.OrderViewsets)

app_name = 'rinconDelSabor'

urlpatterns = [
    path('api/', include(router.urls)),
    path('', views.Base.as_view(), name='base'),
    path('create-client/', views.CreateClient.as_view(), name='CreateCliente'),
    path('list-clients/', views.ListClient.as_view(), name='listClient'),
    path('update-clients/<int:pk>', views.UpdateClient.as_view(), name='updateClient'),
    path('delete-clients/<int:pk>', views.DeleteClient.as_view(), name='deleteClient'),

    path('create-waiter/', views.CreateWaiter.as_view(), name='createWaiter'),
    path('list-waiter/', views.ListWaiter.as_view(), name='listWaiter'),
    path('update-waiter/<int:pk>', views.UpdateWaiter.as_view(), name='updateWaiter'),
    path('delete-waiter/<int:pk>', views.DeleteWaiter.as_view(), name='deleteWaiter'),

    path('create-product/', views.CreateProduct.as_view(), name='createProduct'),
    path('list-product/', views.ListProduct.as_view(), name='listProduct'),
    path('update-product/<int:pk>', views.UpdateProduct.as_view(), name='updateProduct'),
    path('delete-product/<int:pk>', views.DeleteProduct.as_view(), name='deleteProduct'),

    path('create-order/', views.CreateOrder.as_view(), name='createOrder'),
    path('list-order/', views.ListOrder.as_view(), name='listOrder'),
    path('update-order/<int:pk>', views.UpdateOrder.as_view(), name='updateOrder'),
    path('delete-order/<int:pk>', views.DeleteOrder.as_view(), name='deleteOrder'),

    path('create-type-product/', views.CreateTypeProduct.as_view(), name='createTypeProduct'),
    path('list-type-product/', views.ListTypeProduct.as_view(), name='listTypeProduct'),
    path('update-type-product/<int:pk>', views.UpdateTypeProduct.as_view(), name='updateTypeProduct'),
    path('delete-type-product/<int:pk>', views.DeleteTypeProduct.as_view(), name='deleteTypeProduct'),

    path('create-table/', views.CreateTable.as_view(), name='createTable'),
    path('list-table/', views.ListTable.as_view(), name='listTable'),
    path('update-table/<int:pk>', views.UpdateTable.as_view(), name='updateTable'),
    path('delete-table/<int:pk>', views.DeleteTable.as_view(), name='deleteTable'),

    path('create-facture/', views.CreateFacture.as_view(), name='createFacture'),
    path('list-facture/', views.ListFacture.as_view(), name='listFacture'),
    path('update-facture/<int:pk>', views.UpdateFacture.as_view(), name='updateFacture'),
    path('delete-facture/<int:pk>', views.DeleteFacture.as_view(), name='deleteFacture'),

]
