import order
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, TemplateView
from django.views.generic.edit import CreateView, UpdateView
from rest_framework import viewsets, permissions
from rinconDelSabor import serializers
# Create your views here.
from rinconDelSabor.models import Client, Waiter, Table, TypeProduct, Product, Facture, Order


class Base(TemplateView):
    template_name = 'base.html'


# ----Client
class ClientViewsets(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = serializers.ClientSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]


class CreateClient(CreateView):
    model = Client
    fields = ['first_name', 'last_name', 'observations']
    template_name = 'client/createClient.html'
    success_url = reverse_lazy('rinconDelSabor:base')


class ListClient(ListView):
    queryset = Client.objects.filter(delete_client=False)
    template_name = 'client/listClient.html'
    paginate_by = 15


class UpdateClient(UpdateView):
    model = Client
    pk_url_kwarg = 'pk'
    fields = ['first_name', 'last_name', 'observations']
    template_name = 'client/updateClient.html'
    success_url = reverse_lazy('rinconDelSabor:listClient')


class DeleteClient(UpdateClient):
    model = Client
    pk_url_kwarg = 'pk'
    fields = ['delete_client']
    template_name = 'client/deleteClient.html'
    success_url = reverse_lazy('rinconDelSabor:listClient')


# -----------Waiter
class WaiterViewsets(viewsets.ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = serializers.WaiterSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]


class CreateWaiter(CreateView):
    model = Waiter
    fields = ['first_name', 'last_name', 'last_name2']
    template_name = 'waiter/createWaiter.html'
    success_url = reverse_lazy('rinconDelSabor:listWaiter')


class ListWaiter(ListView):
    queryset = Waiter.objects.filter(delete_waiter=False)
    template_name = 'waiter/listWaiter.html'
    paginate_by = 15


class UpdateWaiter(UpdateView):
    model = Waiter
    fields = ['first_name', 'last_name', 'last_name2']
    template_name = 'waiter/updateWaiter.html'
    success_url = reverse_lazy('rinconDelSabor:listWaiter')


class DeleteWaiter(UpdateClient):
    model = Waiter
    fields = ['delete_waiter']
    template_name = 'waiter/deleteWaiter.html'
    success_url = reverse_lazy('rinconDelSabor:listWaiter')


# ---------------table
class TableViewsets(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = serializers.TableSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]


class CreateTable(CreateView):
    model = Table
    fields = ['num_clients', 'location']
    template_name = 'table/createTable.html'
    success_url = reverse_lazy('rinconDelSabor:listTable')


class ListTable(ListView):
    queryset = Table.objects.filter(delete_table=False)
    template_name = 'table/listTable.html'
    paginate_by = 15


class UpdateTable(UpdateView):
    model = Table
    fields = ['num_clients', 'location']
    template_name = 'table/updateTable.html'
    success_url = reverse_lazy('rinconDelSabor:listTable')


class DeleteTable(UpdateClient):
    model = Table
    fields = ['delete_table']
    template_name = 'table/deleteTable.html'
    success_url = reverse_lazy('rinconDelSabor:listTable')


# # ---------------type product
class TypeProductViewsets(viewsets.ModelViewSet):
    queryset = TypeProduct.objects.all()
    serializer_class = serializers.TypeProductSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]


class CreateTypeProduct(CreateView):
    model = TypeProduct
    fields = ['type']
    template_name = 'typeProduct/createTypeProduct.html'
    success_url = reverse_lazy('rinconDelSabor:listTypeProduct')


class ListTypeProduct(ListView):
    queryset = TypeProduct.objects.filter(delete_type_product=False)
    template_name = 'typeProduct/listTypeProduct.html'
    paginate_by = 15


class UpdateTypeProduct(UpdateView):
    model = TypeProduct
    fields = ['type']
    template_name = 'typeProduct/updateTypeProduct.html'
    success_url = reverse_lazy('rinconDelSabor:listTypeProduct')


class DeleteTypeProduct(UpdateClient):
    model = TypeProduct
    fields = ['delete_type_product']
    template_name = 'typeProduct/deleteTypeProduct.html'
    success_url = reverse_lazy('rinconDelSabor:listTypeProduct')


# ---------------product
class ProductViewsets(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = serializers.ProductSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]


class CreateProduct(CreateView):
    model = Product
    fields = ['name', 'type', 'importer']
    template_name = 'product/createProduct.html'
    success_url = reverse_lazy('rinconDelSabor:listProduct')


class ListProduct(ListView):
    queryset = Product.objects.filter(delete_product=False)
    template_name = 'product/listProduct.html'
    paginate_by = 15


class UpdateProduct(UpdateView):
    model = Product
    fields = ['name', 'type', 'importer']
    template_name = 'product/updateProduct.html'
    success_url = reverse_lazy('rinconDelSabor:listProduct')


class DeleteProduct(UpdateClient):
    model = Product
    fields = ['delete_product']
    template_name = 'product/deleteProduct.html'
    success_url = reverse_lazy('rinconDelSabor:listProduct')


# --------------- facture
class FactureViewsets(viewsets.ModelViewSet):
    queryset = Facture.objects.all()
    serializer_class = serializers.FactureSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]


class CreateFacture(CreateView):
    model = Facture
    fields = ['id_client', 'id_waiter', 'id_table']
    template_name = 'facture/createFacture.html'
    success_url = reverse_lazy('rinconDelSabor:listFacture')


class ListFacture(ListView):
    queryset = Facture.objects.filter(delete_facture=False)
    template_name = 'facture/listFacture.html'
    paginate_by = 15


class UpdateFacture(UpdateView):
    model = Facture
    fields = ['id_client', 'id_waiter', 'id_table']
    template_name = 'facture/updateFacture.html'
    success_url = reverse_lazy('rinconDelSabor:listFacture')


class DeleteFacture(UpdateClient):
    model = Facture
    fields = ['delete_facture']
    template_name = 'facture/deleteFacture.html'
    success_url = reverse_lazy('rinconDelSabor:listFacture')


# ---------------order
class OrderViewsets(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = serializers.OrderSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly
    ]


class CreateOrder(CreateView):
    model = Order
    fields = ['product', 'quantity', 'id_facture', 'id_table']
    template_name = 'order/createOrder.html'
    success_url = reverse_lazy('rinconDelSabor:listOrder')


class ListOrder(ListView):
    queryset = Order.objects.filter(delete_order=False)
    template_name = 'order/listOrder.html'
    paginate_by = 15


class UpdateOrder(UpdateView):
    model = Order
    fields = ['product', 'quantity', 'id_facture', 'id_table']
    template_name = 'order/updateOrder.html'
    success_url = reverse_lazy('rinconDelSabor:listOrder')


class DeleteOrder(UpdateClient):
    model = Order
    fields = ['delete_order']
    template_name = 'order/deleteOrder.html'
    success_url = reverse_lazy('')
    success_url = reverse_lazy('rinconDelSabor:listOrder')
